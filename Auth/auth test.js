// basic
github.authenticate({
   type: "basic".
   username: USERNAME,
   password: PASSWORD
});

// oauth
github.authenticate({
      type: "oauth",
      token: AUTH_TOKEN
});

// oauth key/secret (to get a token)
github.authenticate({
        type: "oauth",
        key: CLIENT_ID,
        secret: CLIENT_SECRET
});

// user token
github.authenticate({
        type: "token",
        token: "userToken",
});

// integration(jwt)
github.authenticate({
       type: "integration",
       token: "jwt",
});

// ~/.netrc
github.authenticate({
      type:   "netrc"
});

// creating a token for your application

github.authorization.create({
scopes: ["user", "public_repo", "repo", "repo:status", "gist"]
note: "what this auth is for"
note_url: "http://url-to-this-auth-app",
headers: {
     "X-GitHub-OTP": "two-factor-code"
     }
}, function(err, res) {
 if (res.token){

 }

}

});

// create test auth file

$ > testAuth.json
{
 "token" : "<TOKEN>"
}

    user code

// Resetting the password and using account email as unique lookup id

$(document).ready(function(){

	var rv = new ResetValidator();

	$('#set-password-form').ajaxForm({
		beforeSubmit : function(formData, jqForm, options){;
			rv.hideAlert();
			if (rv.validatePassword($('#pass-tf').val()) == false){
				return false;
			} 	else{
				return true;
			}
		},
		success	: function(responseText, status, xhr, $form){
			rv.showSuccess("Your password has been reset.");
			setTimeout(function(){ window.location.href = '/'; }, 3000);
		},
		error : function(){
			rv.showAlert("I'm sorry something went wrong, please try again.");
		}
	});

	$('#set-password').modal('show');
	$('#set-password').on('shown', function(){ $('#pass-tf').focus(); })

});

// Customizing sign-up page

$(document).ready(function(){

	var av = new AccountValidator();
	var sc = new SignupController();

	$('#account-form').ajaxForm({
		beforeSubmit : function(formData, jqForm, options){
			return av.validateForm();
		},
		success	: function(responseText, status, xhr, $form){
			if (status == 'success') $('.modal-alert').modal('show');
		},
		error : function(e){
			if (e.responseText == 'email-taken'){
			    av.showInvalidEmail();
			}	else if (e.responseText == 'username-taken'){
			    av.showInvalidUserName();
			}
		}
	});
	$('#name-tf').focus();

// customize the account signup form //

	$('#account-form h2').text('Signup');
	$('#account-form #sub1').text('Please tell us a little about yourself');
	$('#account-form #sub2').text('Choose your username & password');
	$('#account-form-btn1').html('Cancel');
	$('#account-form-btn2').html('Submit');
	$('#account-form-btn2').addClass('btn-primary');

// setup the alert that displays when an account is successfully created //

	$('.modal-alert').modal({ show:false, keyboard : false, backdrop : 'static' });
	$('.modal-alert .modal-header h4').text('Account Created!');
	$('.modal-alert .modal-body p').html('Your account has been created.</br>Click OK to return to the login page.');

});

// Validating login page

function LoginValidator()
{
//  a simple alert window to this controller to display any errors //
	this.loginErrors = $('.modal-alert');

	this.showLoginError = function(t, m)
	{
		$('.modal-alert .modal-header h4').text(t);
		$('.modal-alert .modal-body').html(m);
		this.loginErrors.modal('show');
	}
}

LoginValidator.prototype.validateForm = function()
{
	if ($('#user-tf').val() == ''){
		this.showLoginError('Whoops!', 'Please enter a valid username');
		return false;
	}	else if ($('#pass-tf').val() == ''){
		this.showLoginError('Whoops!', 'Please enter a valid password');
		return false;
	}	else{
		return true;
	}
}

// listing users
(function() {
  var Gitlab, credentials, gitlab;

  process.stdout.write('\u001B[2J\u001B[0;0f');

  Gitlab = require('..');

  credentials = require('./credentials');

  gitlab = new Gitlab({
    token: credentials.token,
    url: credentials.url
  });

  gitlab.users.all(function(users) {
    var i, len, results, user;
    results = [];
    for (i = 0, len = users.length; i < len; i++) {
      user = users[i];
      results.push(console.log("#" + user.id + ": " + user.email + ", " + user.name + ", " + user.created_at));
    }
    return results;
  });

}).call(this);

// Show files
(function() {
  var Gitlab, credentials, gitlab, projectId;

  process.stdout.write('\u001B[2J\u001B[0;0f');

  Gitlab = require('..');

  credentials = require('./credentials');

  gitlab = new Gitlab({
    url: credentials.url,
    token: credentials.token
  });

  projectId = parseInt(process.argv[2]);

  gitlab.projects.repository.showFile({
    projectId: projectId,


    ref: 'master',
    file_path: 'README.md'
  }, function(file) {
    console.log;
    console.log("=== File ===");
    console.log(file);
    if (file) {
      console.log;
      console.log("=== Content ===");
      return console.log((new Buffer(file.content, 'base64')).toString());
    }
  });

}).call(this);

// Reset credentials
(function() {
  var Gitlab, credentials, gitlab;

  process.stdout.write('\u001B[2J\u001B[0;0f');

  Gitlab = require('..');

  credentials = require('./credentials');

  gitlab = new Gitlab({
    url: credentials.url,
    token: credentials.token
  });

  gitlab.projects.all(function(projects) {
    var _project, i, len, results;
    results = [];
    for (i = 0, len = projects.length; i < len; i++) {
      _project = projects[i];
      results.push((function() {
        var project;
        project = _project;
        return gitlab.projects.hooks.list(project.id, function(hooks) {
          var hook, j, len1, url;
          url = "" + credentials.service_hook_base + project.path_with_namespace;
          if (hooks.length > 1) {
            return console.log(url + " too much hooks");
          } else if (hooks.length === 1) {
            for (j = 0, len1 = hooks.length; j < len1; j++) {
              hook = hooks[j];
              if (hook.url !== url) {
                gitlab.projects.hooks.remove(project.id, hook.id, function(ret) {
                  return console.log(ret);
                });
              }
            }
            return console.log(url + " is already OK");
          } else {
            return gitlab.projects.hooks.add(project.id, url, function() {
              return console.log(url + " has been added");
            });
          }
        });
      })());
    }
    return results;
  });

}).call(this);

// testing server-side

var common = require('../common-tap')
var test = require('tap').test

var npmExec = require.resolve('../../bin/npm-cli.js')
var path = require('path')
var ca = path.resolve(__dirname, '../../node_modules/npm-registry-couchapp')

var which = require('which')

var v = process.versions.node.split('.').map(function (n) { return parseInt(n, 10) })
if (v[0] === 0 && v[1] < 10) {
  console.error(

    process.versions.node
  )
} else {
  which('couchdb', function (er) {
    if (er) {
      console.error( er.message)
    } else {
      runTests()
    }
  })
}

var extend = Object.assign || require('util')._extend

function runTests () {
  var env = extend({ TAP: 1 }, process.env)
  env.npm = npmExec

  env.COVERALLS_REPO_TOKEN = ''

  var opts = {
    cwd: ca,
    stdio: 'inherit'
  }
  common.npm(['install'], opts, function (err, code) {
    if (err) { throw err }
    if (code) {
      return test('need install to work', function (t) {
        t.fail('install failed with: ' + code)
        t.end()
      })
    } else {
      opts = {
        cwd: ca,
        env: env,
        stdio: 'inherit'
      }
      common.npm(['test', '--', '-Rtap', '--no-coverage'], opts, function (err, code) {
        if (err) { throw err }
        if (code) {
          return test('need test to work', function (t) {
            t.fail('test failed with: ' + code)
            t.end()
          })
        }
        opts = {
          cwd: ca,
          env: env,
          stdio: 'inherit'
        }
        common.npm(['prune', '--production'], opts, function (err, code) {
          if (err) { throw err }
          process.exit(code || 0)
        })
      })
    }
  })
}

// checking os requests

'use strict'
var path = require('path')
var fs = require('fs')
var test = require('tap').test
var osenv = require('osenv')
var mkdirp = require('mkdirp')
var rimraf = require('rimraf')
var common = require('../common-tap.js')

var base = path.join(__dirname, path.basename(__filename, '.js'))
var installFrom = path.join(base, 'from')
var installIn = path.join(base, 'in')

var json = {
  name: 'check-os-reqs',
  version: '0.0.1',
  description: 'fixture',
  os: ['fake-os']
}

test('setup', function (t) {
  setup()
  t.end()
})

var INSTALL_OPTS = ['--loglevel', 'silly']
var EXEC_OPTS = {cwd: installIn}

test('install bad os', function (t) {
  common.npm(['install', installFrom].concat(INSTALL_OPTS), EXEC_OPTS, function (err, code) {
    t.ifError(err, 'npm ran without issue')
    t.is(code, 1, 'npm install refused to install a package in itself')
    t.end()
  })
})
test('force install bad os', function (t) {
  common.npm(['install', '--force', installFrom].concat(INSTALL_OPTS), EXEC_OPTS, function (err, code) {
    t.ifError(err, 'npm ran without issue')
    t.is(code, 0, 'npm install happily installed a package in itself with --force')
    t.end()
  })
})

test('cleanup', function (t) {
  cleanup()
  t.end()
})

function cleanup () {
  process.chdir(osenv.tmpdir())
  rimraf.sync(base)
}

function setup () {
  cleanup()
  mkdirp.sync(path.resolve(installFrom, 'node_modules'))
  fs.writeFileSync(
    path.join(installFrom, 'package.json'),
    JSON.stringify(json, null, 2)
  )
  mkdirp.sync(path.resolve(installIn, 'node_modules'))
  process.chdir(base)
}


// accessing files

var fs = require('fs')
var path = require('path')
var mkdirp = require('mkdirp')
var rimraf = require('rimraf')
var mr = require('npm-registry-mock')

var test = require('tap').test
var common = require('../common-tap.js')

var pkg = path.resolve(__dirname, 'access')
var server

var scoped = {
  name: '@scoped/pkg',
  version: '1.1.1'
}

test('setup', function (t) {
  mkdirp(pkg, function (er) {
    t.ifError(er, pkg + ' made successfully')

    mr({port: common.port}, function (err, s) {
      t.ifError(err, 'registry mocked successfully')
      server = s

      fs.writeFile(
        path.join(pkg, 'package.json'),
        JSON.stringify(scoped),
        function (er) {
          t.ifError(er, 'wrote package.json')
          t.end()
        }
      )
    })
  })
})

test('npm access public on current package', function (t) {
  server.post('/-/package/%40scoped%2Fpkg/access', JSON.stringify({
    access: 'public'
  })).reply(200, {
    accessChanged: true
  })
  common.npm(
    [
      'access',
      'public',
      '--registry', common.registry,
      '--loglevel', 'silent'
    ], {
      cwd: pkg
    },
    function (er, code, stdout, stderr) {
      t.ifError(er, 'npm access')
      t.equal(code, 0, 'exited OK')
      t.equal(stderr, '', 'no error output')
      t.end()
    }
  )
})

test('npm access public when no package passed and no package.json', function (t) {
  // need to simulate a missing package.json
  var missing = path.join(__dirname, 'access-public-missing-guard')
  mkdirp.sync(path.join(missing, 'node_modules'))

  common.npm([
    'access',
    'public',
    '--registry', common.registry
  ], {
    cwd: missing
  },
  function (er, code, stdout, stderr) {
    t.ifError(er, 'npm access')
    t.match(stderr, /no package name passed to command and no package.json found/)
    rimraf.sync(missing)
    t.end()
  })
})

test('npm access public when no package passed and invalid package.json', function (t) {
  // need to simulate a missing package.json
  var invalid = path.join(__dirname, 'access-public-invalid-package')
  mkdirp.sync(path.join(invalid, 'node_modules'))
  // it's hard to force `read-package-json` to break w/o ENOENT, but this will do it
  fs.writeFileSync(path.join(invalid, 'package.json'), '{\n')

  common.npm([
    'access',
    'public',
    '--registry', common.registry
  ], {
    cwd: invalid
  },
  function (er, code, stdout, stderr) {
    t.ifError(er, 'npm access')
    t.match(stderr, /Failed to parse json/)
    rimraf.sync(invalid)
    t.end()
  })
})

test('npm access restricted on current package', function (t) {
  server.post('/-/package/%40scoped%2Fpkg/access', JSON.stringify({
    access: 'restricted'
  })).reply(200, {
    accessChanged: true
  })
  common.npm(
    [
      'access',
      'restricted',
      '--registry', common.registry,
      '--loglevel', 'silent'
    ], {
      cwd: pkg
    },
    function (er, code, stdout, stderr) {
      t.ifError(er, 'npm access')
      t.equal(code, 0, 'exited OK')
      t.equal(stderr, '', 'no error output')
      t.end()
    }
  )
})

test('npm access on named package', function (t) {
  server.post('/-/package/%40scoped%2Fanother/access', {
    access: 'public'
  }).reply(200, {
    accessChaged: true
  })
  common.npm(
    [
      'access',
      'public', '@scoped/another',
      '--registry', common.registry,
      '--loglevel', 'silent'
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ifError(er, 'npm access')
      t.equal(code, 0, 'exited OK')
      t.equal(stderr, '', 'no error output')

      t.end()
    }
  )
})

test('npm change access on unscoped package', function (t) {
  common.npm(
    [
      'access',
      'restricted', 'yargs',
      '--registry', common.registry
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ok(code, 'exited with Error')
      t.matches(
        stderr, /access commands are only accessible for scoped packages/)
      t.end()
    }
  )
})

test('npm access grant read-only', function (t) {
  server.put('/-/team/myorg/myteam/package', {
    permissions: 'read-only',
    package: '@scoped/another'
  }).reply(201, {
    accessChaged: true
  })
  common.npm(
    [
      'access',
      'grant', 'read-only',
      'myorg:myteam',
      '@scoped/another',
      '--registry', common.registry
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ifError(er, 'npm access grant')
      t.equal(code, 0, 'exited with Error')
      t.end()
    }
  )
})

test('npm access grant read-write', function (t) {
  server.put('/-/team/myorg/myteam/package', {
    permissions: 'read-write',
    package: '@scoped/another'
  }).reply(201, {
    accessChaged: true
  })
  common.npm(
    [
      'access',
      'grant', 'read-write',
      'myorg:myteam',
      '@scoped/another',
      '--registry', common.registry
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ifError(er, 'npm access grant')
      t.equal(code, 0, 'exited with Error')
      t.end()
    }
  )
})

test('npm access grant others', function (t) {
  common.npm(
    [
      'access',
      'grant', 'rerere',
      'myorg:myteam',
      '@scoped/another',
      '--registry', common.registry
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ok(code, 'exited with Error')
      t.matches(stderr, /read-only/)
      t.matches(stderr, /read-write/)
      t.end()
    }
  )
})

test('npm access revoke', function (t) {
  server.delete('/-/team/myorg/myteam/package', {
    package: '@scoped/another'
  }).reply(200, {
    accessChaged: true
  })
  common.npm(
    [
      'access',
      'revoke',
      'myorg:myteam',
      '@scoped/another',
      '--registry', common.registry
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ifError(er, 'npm access grant')
      t.equal(code, 0, 'exited with Error')
      t.end()
    }
  )
})

test('npm access ls-packages with no team', function (t) {
  var serverPackages = {
    '@foo/bar': 'write',
    '@foo/util': 'read'
  }
  var clientPackages = {
    '@foo/bar': 'read-write',
    '@foo/util': 'read-only'
  }
  server.get(
    '/-/org/username/package?format=cli'
  ).reply(200, serverPackages)
  common.npm(
    [
      'access',
      'ls-packages',
      '--registry', common.registry
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ifError(er, 'npm access ls-packages')
      t.same(JSON.parse(stdout), clientPackages)
      t.end()
    }
  )
})

test('npm access ls-packages on team', function (t) {
  var serverPackages = {
    '@foo/bar': 'write',
    '@foo/util': 'read'
  }
  var clientPackages = {
    '@foo/bar': 'read-write',
    '@foo/util': 'read-only'
  }
  server.get(
    '/-/team/myorg/myteam/package?format=cli'
  ).reply(200, serverPackages)
  common.npm(
    [
      'access',
      'ls-packages',
      'myorg:myteam',
      '--registry', common.registry
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ifError(er, 'npm access ls-packages')
      t.same(JSON.parse(stdout), clientPackages)
      t.end()
    }
  )
})

test('npm access ls-packages on org', function (t) {
  var serverPackages = {
    '@foo/bar': 'write',
    '@foo/util': 'read'
  }
  var clientPackages = {
    '@foo/bar': 'read-write',
    '@foo/util': 'read-only'
  }
  server.get(
    '/-/org/myorg/package?format=cli'
  ).reply(200, serverPackages)
  common.npm(
    [
      'access',
      'ls-packages',
      'myorg',
      '--registry', common.registry
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ifError(er, 'npm access ls-packages')
      t.same(JSON.parse(stdout), clientPackages)
      t.end()
    }
  )
})

test('npm access ls-packages on user', function (t) {
  var serverPackages = {
    '@foo/bar': 'write',
    '@foo/util': 'read'
  }
  var clientPackages = {
    '@foo/bar': 'read-write',
    '@foo/util': 'read-only'
  }
  server.get(
    '/-/org/myorg/package?format=cli'
  ).reply(404, {error: 'nope'})
  server.get(
    '/-/user/myorg/package?format=cli'
  ).reply(200, serverPackages)
  common.npm(
    [
      'access',
      'ls-packages',
      'myorg',
      '--registry', common.registry
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ifError(er, 'npm access ls-packages')
      t.same(JSON.parse(stdout), clientPackages)
      t.end()
    }
  )
})

test('npm access ls-packages with no package specified or package.json', function (t) {

  var missing = path.join(__dirname, 'access-missing-guard')
  mkdirp.sync(path.join(missing, 'node_modules'))

  var serverPackages = {
    '@foo/bar': 'write',
    '@foo/util': 'read'
  }
  var clientPackages = {
    '@foo/bar': 'read-write',
    '@foo/util': 'read-only'
  }
  server.get(
    '/-/org/myorg/package?format=cli'
  ).reply(404, {error: 'nope'})
  server.get(
    '/-/user/myorg/package?format=cli'
  ).reply(200, serverPackages)
  common.npm(
    [
      'access',
      'ls-packages',
      'myorg',
      '--registry', common.registry
    ],
    { cwd: missing },
    function (er, code, stdout, stderr) {
      t.ifError(er, 'npm access ls-packages')
      t.same(JSON.parse(stdout), clientPackages)
      rimraf.sync(missing)
      t.end()
    }
  )
})

test('npm access ls-collaborators on current', function (t) {
  var serverCollaborators = {
    'myorg:myteam': 'write',
    'myorg:anotherteam': 'read'
  }
  var clientCollaborators = {
    'myorg:myteam': 'read-write',
    'myorg:anotherteam': 'read-only'
  }
  server.get(
    '/-/package/%40scoped%2Fpkg/collaborators?format=cli'
  ).reply(200, serverCollaborators)
  common.npm(
    [
      'access',
      'ls-collaborators',
      '--registry', common.registry
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ifError(er, 'npm access ls-collaborators')
      t.same(JSON.parse(stdout), clientCollaborators)
      t.end()
    }
  )
})

test('npm access ls-collaborators on package', function (t) {
  var serverCollaborators = {
    'myorg:myteam': 'write',
    'myorg:anotherteam': 'read'
  }
  var clientCollaborators = {
    'myorg:myteam': 'read-write',
    'myorg:anotherteam': 'read-only'
  }
  server.get(
    '/-/package/%40scoped%2Fanother/collaborators?format=cli'
  ).reply(200, serverCollaborators)
  common.npm(
    [
      'access',
      'ls-collaborators',
      '@scoped/another',
      '--registry', common.registry
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ifError(er, 'npm access ls-collaborators')
      t.same(JSON.parse(stdout), clientCollaborators)
      t.end()
    }
  )
})

test('npm access ls-collaborators on current w/user filter', function (t) {
  var serverCollaborators = {
    'myorg:myteam': 'write',
    'myorg:anotherteam': 'read'
  }
  var clientCollaborators = {
    'myorg:myteam': 'read-write',
    'myorg:anotherteam': 'read-only'
  }
  server.get(
    '/-/package/%40scoped%2Fanother/collaborators?format=cli&user=zkat'
  ).reply(200, serverCollaborators)
  common.npm(
    [
      'access',
      'ls-collaborators',
      '@scoped/another',
      'zkat',
      '--registry', common.registry
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ifError(er, 'npm access ls-collaborators')
      t.same(JSON.parse(stdout), clientCollaborators)
      t.end()
    }
  )
})

test('npm access edit', function (t) {
  common.npm(
    [
      'access',
      'edit', '@scoped/another',
      '--registry', common.registry
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ok(code, 'exited with Error')
      t.match(stderr, /edit subcommand is not implemented yet/)
      t.end()
    }
  )
})

test('npm access blerg', function (t) {
  common.npm(
    [
      'access',
      'blerg', '@scoped/another',
      '--registry', common.registry
    ],
    { cwd: pkg },
    function (er, code, stdout, stderr) {
      t.ok(code, 'exited with Error')
      t.matches(stderr, /Usage:/)
      t.end()
    }
  )
})

test('cleanup', function (t) {
  t.pass('cleaned up')
  rimraf.sync(pkg)
  server.done()
  server.close()
  t.end()
})

// adding projects in a group

gitlab.groups.addProject = function(groupId, projectId, fn) {
      if (fn == null) {
        fn = null;
      }
      this.debug("Groups::addProject(" + groupId + ", " + projectId + ")");
      return this.post("groups/" + (parseInt(groupId)) + "/projects/" + (parseInt(projectId)), null, function(data) {
        if (fn) {
          return fn(data);
        }
      });
    };

// adding members in a group

gitlab.groups.addMember = function(groupId, userId, accessLevel, fn) {
      var checkAccessLevel, params;
      if (fn == null) {
        fn = null;
      }
      this.debug("addMember(" + groupId + ", " + userId + ", " + accessLevel + ")");
      checkAccessLevel = (function(_this) {
        return function() {
          var access_level, k, ref;
          ref = _this.access_levels;
          for (k in ref) {
            access_level = ref[k];
            if (accessLevel === access_level) {
              return true;
            }
          }
          return false;
        };
      })(this);
      if (!checkAccessLevel()) {
        throw "`accessLevel` must be one of " + (JSON.stringify(this.access_levels));
      }
      params = {
        user_id: userId,
        access_level: accessLevel
      };
      return this.post("groups/" + (parseInt(groupId)) + "/members", params, function(data) {
        if (fn) {
          return fn(data);
        }
      });
    };

    // searching groups

    gitlab.groups.search = function(nameOrPath, fn) {
      var params;
      if (fn == null) {
        fn = null;
      }
      this.debug("Groups::search(" + nameOrPath + ")");
      params = {
        search: nameOrPath
      };
      return this.get("groups", params, function(data) {
        if (fn) {
          return fn(data);
        }
      });
    };

// creating assignment in a course

const Config = require('../../../node_modules/electron-config');
const config = new Config();
var Gitlab = require('../../../node_modules/gitlab');
module.exports = function (fn) {

    var gitlab = new Gitlab({
      url: "http://csgrad06.nwmissouri.edu",
        token: config.get('private_token')
    });
    //console.log("Before all() call")
    var returnedResults = [];
    gitlab.courses.all(function (courses) {
        var i, len, course, results;
        results = []
        for (i = 0, len = courses.length; i < len; i++) {
            course = courses[i];
            var split_result = split_namespace_result(course.path_with_namespace)
            results.push({ 'id': course.id, 'course_name': course.name, 'path_with_namespace': course.path_with_namespace, 'ssh_url': course.ssh_url_to_repo, 'split_namespace_result': split_result, 'created_at': course.created_at });
            console.log(results[i])
        }

        // console.log("In for"+returnedResults.length)
        returnedResults = results;
        callbackResults(returnedResults)
        // return results;
    })
    function callbackResults(returnedResults) {
        // console.log("After all() call")
        //console.log(returnedResults.length)
        fn(returnedResults);
    }
    function split_namespace_result(namespace) {

        var semester = "";
        var project = "";
        var course = "";
        var section = "";
        var courseName = "";

        if (namespace.split('-').length <= 1) {
            console.log("Inapproppriate namespace URL. Sending emtpy strings for semester,project,course.")
        }
        else {


            project = namespace.split('/')[1]
            semester = namespace.split('-')[0]
            course = namespace.split('-')[1]
            section = namespace.split('.')[0].split('-')[2]

            courseName = namespace.split('.')[1].split('/')[0]
        }
        console.log(semester, course, section, courseName, project)
        return { 'semester': semester, 'course': course, 'section': section, 'courseName': courseName, 'project': project }

    }
    // console.log("I am out")
    //EXCEUTES FIRST

    // returnedResults.push("Hello");


// checking the path

var main = module.exports = function() {
    // check routes path
    var routesPath = Path.join(__dirname, "routes.json");
    var routes = JSON.parse(fs.readFileSync(routesPath, "utf8"));
    if (!routes.defines) {
        Util.log("No routes defined.", "fatal");
        process.exit(1);
    }

    Util.log("Generating...");
