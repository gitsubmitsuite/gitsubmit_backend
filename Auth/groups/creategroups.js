



gitlab.groups.create = function(name,path,description  fn) {
      if (name, path == null) {
        name, path = {};
      }
      if (fn == null) {
        fn = null;
      }
      this.debug("Groups::create()");
      return this.post("groups", name,path  function(data) {
        if (fn) {
          return fn(data);
        }
      });
    };
