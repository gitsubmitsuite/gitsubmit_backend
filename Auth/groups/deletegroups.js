

gitlab.groups.deleteGroup = function(groupId, fn) {
      if (fn == null) {
        fn = null;
      }
      this.debug("Groups::delete(" + groupId + ")");
      return this["delete"]("groups/" + (parseInt(groupId)), function(data) {
        if (fn) {
          return fn(data);
        }
      });
    };
