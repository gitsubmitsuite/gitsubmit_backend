



gitlab.groups.listProjects = function(groupId, order_by, sort, fn) {
      if (fn == null) {
        fn = null;
      }
      this.debug("Groups::listProjects()");
      return this.get("groups/" + (parseInt(groupId)), (function(_this) {
        return function(data) {
          if (fn) {
            return fn(data.projects);
          }
        };
      })(this));
    };
