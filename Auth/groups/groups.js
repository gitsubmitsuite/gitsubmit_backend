const Config = require('../../../node_modules/electron-config');
const config = new Config();
var Gitlab = require('../../../node_modules/gitlab');
module.exports = function (fn) {
    //console.log("I am in list-projects")
    var gitlab = new Gitlab({
        url: "http://csgrad06.nwmissouri.edu",
        token: config.get('private_token')
    });
    //console.log("Before all() call")
	var returnedResults = [];
    gitlab.projects.all(function (projects) {
        var i, len, project, results;
        results = []
        for (i = 0, len = projects.length; i < len; i++) {
            project = projects[i];
            var split_result = split_namespace_result(project.path_with_namespace)
            results.push({ 'id': project.id, 'project_name': project.name, 'path_with_namespace': project.path_with_namespace, 'ssh_url': project.ssh_url_to_repo, 'split_namespace_result': split_result, 'created_at': project.created_at });
            console.log(results[i])
        }
		(function() {
  var BaseModel, Groups,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  BaseModel = require('../BaseModel');

  Groups = (function(superClass) {
    extend(Groups, superClass);

    function Groups() {
      this.search = bind(this.search, this);
      this.deleteGroup = bind(this.deleteGroup, this);
      this.addProject = bind(this.addProject, this);
      this.create = bind(this.create, this);
      this.listProjects = bind(this.listProjects, this);
      this.show = bind(this.show, this);
      this.all = bind(this.all, this);
      this.init = bind(this.init, this);
      return Groups.__super__.constructor.apply(this, arguments);
    }
	
	
  Groups.project.listProjects = function(groupId, fn) {
      if (fn == null) {
        fn = null;
      }
      this.debug("Groups::listProjects()");
      return this.get("groups/" + (parseInt(groupId)), (function(_this) {
        return function(data) {
          if (fn) {
            return fn(data.projects);
          }
        };
      })(this));
    };
    
        

    
    // console.log("I am out")
    //EXCEUTES FIRST

    // returnedResults.push("Hello");


}
