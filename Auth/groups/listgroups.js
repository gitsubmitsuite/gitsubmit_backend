


gitlab.groups.show = function(groupId, search, order_by, sort, fn) {
     if (fn == null) {
       fn = null;
     }
     this.debug("Groups::show()");
     return this.get("groups/" + (parseInt(groupId)), (function(_this) {
       return function(data) {
         if (fn) {
           return fn(data);
         }
       };
     })(this));
   };
