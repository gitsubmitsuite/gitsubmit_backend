

gitlab.groups.search = function(nameOrPath, id, description  fn) {
      var params;
      if (fn == null) {
        fn = null;
      }
      this.debug("Groups::search(" + nameOrPath + id + description + ")");
      params = {
        search: nameOrPath + id + description
      };
      return this.get("groups", params, function(data) {
        if (fn) {
          return fn(data);
        }
      });
    };
