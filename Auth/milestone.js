var keypair = require('../../../node_modules/keypair');
const Config = require('../../../node_modules/electron-config');
const config = new Config();
var Gitlab = require('../../../node_modules/gitlab');
var fse = require("../../../node_modules/fs-extra");
var path = require("path");
var forge = require('../../../node_modules/node-forge');

module.exports = function () {
    var gitlab = new Gitlab({
        url: "http://csgrad06.nwmissouri.edu",
        token: config.get('private_token')
    });
//console.log("Before all() call")
    ProjectMilestones.gitlab.add = function(projectId, title, description, due_date, fn) {
      var params;
      if (fn == null) {
        fn = null;
      }
      this.debug("Projects::addMilestone()");
      params = {
        id: Utils.parseProjectId(projectId),
        title: title,
        description: description,
        due_date: due_date
      };
      return this.post("projects/" + (Utils.parseProjectId(projectId)) + "/milestones", params, (function(_this) {
        return function(data) {
          if (fn) {
            return fn(data);
          }
        };
      })(this));
    };

    // console.log("In for"+returnedResults.length)
    returnedResults = results;
    callbackResults(returnedResults)
    // return results;
})
function callbackResults(returnedResults) {
    // console.log("After all() call")
    //console.log(returnedResults.length)
    fn(returnedResults);
}

// console.log("I am out")
//EXCEUTES FIRST

// returnedResults.push("Hello");

}
