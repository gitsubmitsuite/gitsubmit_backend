// creating a file in ProjectRepository
ProjectRepository.createFile = function(params, fn) {
      if (params == null) {
        params = {};
      }
      if (fn == null) {
        fn = null;
      }
      this.debug("Projects::createFile()", params);
      return this.post("projects/" + (Utils.parseProjectId(params.projectId)) + "/repository/files", params, (function(_this) {
        return function(data) {
          if (fn) {
            return fn(data);
          }
        };
      })(this));
    };

// updating a file in ProjectRepository
ProjectRepository.updateFile = function(params, fn) {
     if (params == null) {
       params = {};
     }
     if (fn == null) {
       fn = null;
     }
     this.debug("Projects::updateFile()", params);
     return this.put("projects/" + (Utils.parseProjectId(params.projectId)) + "/repository/files", params, (function(_this) {
       return function(data) {
         if (fn) {
           return fn(data);
         }
       };
     })(this));
   };

// creating a file in ProjectRepository

ProjectRepository.createFile = function(params, fn) {
      if (params == null) {
        params = {};
      }
      if (fn == null) {
        fn = null;
      }
      this.debug("Projects::createFile()", params);
      return this.post("projects/" + (Utils.parseProjectId(params.projectId)) + "/repository/files", params, (function(_this) {
        return function(data) {
          if (fn) {
            return fn(data);
          }
        };
      })(this));
    };
