


gitlab.projectMilestones.update = function(projectId, milestoneId, title, description, due_date,start_date, state_event, fn) {
     var params;
     if (fn == null) {
       fn = null;
     }
     this.debug("Projects::editMilestone()");
     params = {
       id: Utils.parseProjectId(projectId),
       title: title,
       description: description,
       due_date: due_date,
       state_event: state_event
     };
     return this.put("projects/" + (Utils.parseProjectId(projectId)) + "/milestones/" + (parseInt(milestoneId)), params, (function(_this) {
       return function(data) {
         if (fn) {
           return fn(data);
         }
       };
     })(this));
   };

   return ProjectMilestones;
