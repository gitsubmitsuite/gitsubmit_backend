var Git = require("../../../node_modules/nodegit");
var path = require('path')
const Config = require('../../../node_modules/electron-config');
const config = new Config();
var signature;
var repo;
var remote;
//var pushUrl = "git@csgrad06.nwmissouri.edu:F2017-4450.ADB/msardar_gitlabClone.git";
var sshPublicKeyPath = require("path").join(__dirname, "../../../SSHKeys/id_rsa.pub");
var sshPrivateKeyPath = require("path").join(__dirname, "../../../SSHKeys/id_rsa");
var keyPhrase = "empty"
module.exports = function (pushUrl, repo, fn) {
  console.log("Repo location for push is:" + repo + " with pushUrl:" + pushUrl)
  Git.Repository.open(repo)
    .then(function (repoResult) {
      repo = repoResult
      return repo
    })
    .then(function (repository) {
      return repository.getRemote('origin')
        .then(function (remoteResult) {
          remote = remoteResult;

          // Create the push object for this remote
          return remote.push(
            ["refs/heads/master:refs/heads/master"],
            {
              callbacks: {
                credentials: function (pushUrl, userName) {
                  return Git.Cred.sshKeyNew(
                    userName,
                    sshPublicKeyPath,
                    sshPrivateKeyPath,
                    keyPhrase);
                }
              }
            }
          ).then(function () {
            fn(true);
          }).catch(function (err) {
            fn(false)
            console.log("Push error:" + err)
          });
        });

    }).catch(function (err) {
      fn(false)
      console.log("Repository error in push" + err)
    }).done(function () {
      console.log("Done!");

    });


}
