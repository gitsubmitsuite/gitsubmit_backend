var Git = require("../../../node_modules/nodegit");
var path = require('path')
var local = path.join.bind(path, __dirname);
var fileName = 'NewTextFile.txt'
var promisify = require("../../../node_modules/promisify-node");
var fse = promisify(require("../../../node_modules/fs-extra"));
var directoryName = "C:/Users/s525061/Desktop/Notes/GDP/GDP2/ProjectCode/CLONETEST/msardar_gitlabClone"
fse.ensureDir = promisify(fse.ensureDir);


module.exports = function (filesToBeStaged, localRepo, fn) {
    var repo;
    var index;
    var oid;
    var indexHead;
    var indexParent;
    console.log("In add with files:" + filesToBeStaged)
    Git.Repository.open(localRepo)
        .then(function (repoResult) {
            repo = repoResult
            console.log("REFRESHED INDEX in " + repo.workdir())
            return repo.refreshIndex();
        })
        .then(function (indexResult) {
            index = indexResult;

            console.log("The index: " + index)
        })
        .then(function () {
            // this file is in the root of the directory and doesn't need a full path
            return index.addAll(filesToBeStaged);
        })
        .then(function () {
            // this will write both files to the index

            return index.write();
        })
        .then(function () {
            console.log("Writing tree")
            return index.writeTree();
        })
        .then(function (oidResult) {
            oid = oidResult;
            console.log("OID " + oid)
            fn(true, { 'oid': oid })

        })
        .catch(function (err) {
            console.log("Error in add.js" + err)
            fn(false)
        })



}
