
var Git = require("../../../node_modules/nodegit");
var path = require('path')
var local = path.join.bind(path, __dirname);
var open_repo = require('../open_repo')
module.exports = function (repoUrl, folderDestination, project_name, fn) {

  // Remote URL from GitLab server
  //var cloneURL = "git@csgrad06.nwmissouri.edu:F2017-4450.ADB/msardar_gitlabClone.git";
  var cloneURL = repoUrl
  //console.log("URL---------->" + cloneURL)
  // Clone a given repository into the desired folder.
  // var localPath = require("path").join(__dirname, "../../../CLONETEST");
  var localPath = path.join(folderDestination + "//" + project_name)
  //console.log("PATH--------->" + localPath)
  var cloneOptions = {};
  //Set this path to the desired location and make sure both private and public keys exist in the path
  // StudentUI
  // Created a dynamic folder to store SSH keys in app space
  var sshPublicKeyPath = require("path").join(__dirname, "../../../SSHKeys/id_rsa.pub");
  var sshPrivateKeyPath = require("path").join(__dirname, "../../../SSHKeys/id_rsa");
  // //InstructorUI
  // var sshPublicKeyPath = require("path").join(__dirname, "../../../../.ssh/id_rsa.pub");
  // var sshPrivateKeyPath = require("path").join(__dirname, "../../../../.ssh/id_rsa");
  var keyPhrase = "empty"

  //Setting the options for clone
  var opts = {
    fetchOpts: {
      callbacks: {
        certificateCheck: function () {
          return 1;
        },
        credentials: function (cloneURL, userName) {
          return Git.Cred.sshKeyNew(
            userName,
            sshPublicKeyPath,
            sshPrivateKeyPath,
            keyPhrase);
        }
      }
    }
  };
  open_repo(localPath, function (response) {
    //console.log("Open repo response in clone:" + response)
    if (response) {
      fn(false, { message: "Folder exists already and is not an empty directory", empty: false })
    }
    else {
      //Calling clone function
      var cloneRepository = Git.Clone(cloneURL, localPath, opts)
        .then(function () {
          console.log("*********Clone success*********")

          Git.Repository.open(localPath).then(function (repository) {
            if (repository.isEmpty()) {
              fn(true, { message: "Cloned successfully but the repository is empty", empty: true })
            }
            else {
              fn(true, { message: "Clone successful", empty: false })
            }
          })


        }).catch(function (err) {
          console.log(err);
          if (err.toString().indexOf('exists') != -1) {

            fn(false, { message: "Folder exists already and is not an empty directory", empty: false })
          }
          else {
            fn(false, { message: "Error: Cannot clone the repository due to:" + err, empty: false })
          }

        });
    }
  })

}