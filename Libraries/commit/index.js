var Git = require("../../../node_modules/nodegit");
var path = require('path')
var repo;
var index;
const Config = require('../../../node_modules/electron-config');
const config = new Config();
module.exports = function (folder, oid, commitMessage, fn) {
    console.log("In commit message:")
    Git.Repository.open(folder)
        .then(function (repoResult) {
            repo = repoResult
        })
        .then(function () {

            return Git.Reference.nameToId(repo, "HEAD");
        })
        .then(function (head) {
            return repo.getCommit(head);
        })
        .then(function (parent) {
            var author = Git.Signature.now(config.get('name'),
                config.get('email'));
            var committer = Git.Signature.now(config.get('name'),
                config.get('email'));
            console.log(commitMessage)
            return repo.createCommit("HEAD", author, committer, commitMessage, oid, [parent]);
        })
        .catch(function (err) {
            console.log("Error in commit.js" + err)
            fn(false)
        })
        .done(function (commitId) {
            console.log("New Commit: ", commitId);
            fn(true, { 'commitId': commitId })
        });

}