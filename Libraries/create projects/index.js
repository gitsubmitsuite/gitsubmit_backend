const Config = require('../../../node_modules/electron-config');
const config = new Config();
var Gitlab = require('../../../node_modules/gitlab');

//const Config = require('../../node_modules/electron-config');
//const config = new Config();
//var Gitlab = require('/../../node_modules/gitlab');

module.exports = function (fn) {
    //console.log("I am in create projects")

    //credentials = require('./credentials');

    gitlab = new Gitlab({
        url: "http://csgrad06.nwmissouri.edu",
        token: config.get('private_token')
    });
    //console.log("Before all() call")
	
	gitlab.projects.create = function(project_name, fn) {
      if (project_name == null) {
        project_name = {};
      }
      if (fn == null) {
        fn = null;
      }
      this.debug("Projects::create()");
      return this.post("projects", project_name, function(data) {
        if (fn) {
          return fn(data);
        }
      });
    };
    

    }
    // console.log("I am out")
    //EXCEUTES FIRST

    // returnedResults.push("Hello");


}