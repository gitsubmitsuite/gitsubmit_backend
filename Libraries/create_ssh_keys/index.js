var keypair = require('../../../node_modules/keypair');
const Config = require('../../../node_modules/electron-config');
const config = new Config();
var Gitlab = require('../../../node_modules/gitlab');
var fse = require("../../../node_modules/fs-extra");
var path = require("path");
var forge = require('../../../node_modules/node-forge');
var promisify = require("../../../node_modules/promisify-node");

fse.ensureDir = promisify(fse.ensureDir);
module.exports = function () {
    var gitlab = new Gitlab({
        url: "http://csgrad06.nwmissouri.edu",
        token: config.get('private_token')
    });
    var pair = keypair();
    var publicKey = forge.pki.publicKeyFromPem(pair.public);
    var privateKey = forge.pki.privateKeyFromPem(pair.private);
    var passphrase = 'empty';
    var sshPublic = forge.ssh.publicKeyToOpenSSH(publicKey, config.get('email'));
    var sshPrivate = forge.ssh.privateKeyToOpenSSH(privateKey, passphrase);
    //console.log(ssh);
    //console.log(pair);
    var dir = path.join(__dirname, '../../../SSHKeys');
    fse.ensureDir(dir, function () {
        fse.writeFile(path.join(__dirname, '../../../SSHKeys/id_rsa.pub'), sshPublic);
        fse.writeFile(path.join(__dirname, '../../../SSHKeys/id_rsa'), sshPrivate);
        var params = {
            title: "GitSubmitKey",
            key: sshPublic
        };
        gitlab.users.keys.post("user/keys", params, function (data) {

            console.log(data);

        });
    }).catch(function (err) {
        console.log(err)
    })

}


