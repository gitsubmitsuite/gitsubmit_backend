var keypair = require('../../../node_modules/keypair');
const Config = require('../../../node_modules/electron-config');
const config = new Config();
var Gitlab = require('../../../node_modules/gitlab');
var fse = require("../../../node_modules/fs-extra");
var path = require("path");
var forge = require('../../../node_modules/node-forge');
(function() {
  module.exports = {
   url: "http://csgrad06.nwmissouri.edu",
   token: config.get('private_token')
  };

}).call(this)

// for the patform before storing ssh keypair
var isWin = /^win/.test(process.platform);
if(isWin==true)
{
var sshPublicKeyPath = require("path").join(__dirname, "../../../SSHKeys/id_rsa.pub");
var sshPrivateKeyPath = require("path").join(__dirname, "../../../SSHKeys/id_rsa");
}
else
{
var sshPublicKeyPath = require("path").join(__dirname, "..\..\..\SSHKeys\id_rsa.pub");
var sshPrivateKeyPath = require("path").join(__dirname, "..\..\..\SSHKeys\id_rsa");	
	
}	
	