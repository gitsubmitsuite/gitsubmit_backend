const Config = require('../../../node_modules/electron-config');
const config = new Config();
var Gitlab = require('../../../node_modules/gitlab');
module.exports = function (fn) {
    var remote_head;

    var gitlab = new Gitlab({
        url: "http://csgrad06.nwmissouri.edu",
        token: config.get('private_token')
    });
    //console.log("Before all() call")
    var returnedResults = [];
    params = { id: config.get('current_remote_repo_ID') }

    gitlab.projects.listCommits(params, function (commits) {

        remote_head = commits[0];
        fn(remote_head)
    })




}
