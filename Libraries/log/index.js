var Git = require("../../../node_modules/nodegit");
var path = require('path')
const get_head_from_remote = require('../get_head_from_remote')
module.exports = function (folder, last_head, fn) {
    var logObject = [];
    var commitsList = [];
    var remote_head_id;
    var local_head_id;
    var remote_head_commit;
    var local_head_commit;
    var commitsToView = [];
    //var last_head = '3ceb5a957dd7ed121b440f0a39b46ac6cb98ae5c';
    // Open the repository directory.
    // Create a counter to only show up to 9 entries.
    var count = 0;
    get_head_from_remote(function (remote_head) {
        remote_head_commit = remote_head;
        remote_head_id = remote_head.id
        // console.log("Remote Head:" + remote_head.id)
        Git.Repository.open(folder)
            // Open the master branch.

            .then(function (repo) {

                return repo.getMasterCommit();
            })
            // Display information about commits on master.
            .then(function (firstCommitOnMaster) {
                // Create a new history event emitter.
                var history = firstCommitOnMaster.history();


                history.on('end', function (commits) {
                    //console.log("In log:" + logObject.length)
                    if (commitsList.indexOf(remote_head_id) == -1) {
                        //console.log("Need to pull remote: with last_head:" + last_head + " and index of last head" + commitsList.indexOf(last_head))
                        for (var i = 0; i < commitsList.indexOf(last_head); i++) {
                            //console.log("Before head at:" + i + " commit sha:" + commitsList[i])
                            commitsToView.push(logObject[i])
                        }
                        fn(true, commitsToView, remote_head_commit, true, '')
                    }
                    else if (commitsList.indexOf(remote_head_id) == 0) {
                        //console.log("Remote == Local, No changes made")
                        last_head = remote_head_id;

                        fn(true, commitsToView, '', false, last_head)
                    }
                    else {
                        for (var i = 0; i < commitsList.indexOf(remote_head_id); i++) {
                            //console.log("Before head at:" + i + " commit sha:" + commitsList[i])
                            commitsToView.push(logObject[i])
                        }
                        last_head = remote_head_id;
                        fn(true, commitsToView, '', false, last_head)
                    }


                    // Use commits
                });
                // Listen for commit events from the history.
                history.on("commit", function (commit) {
                    // Disregard commits past 5.
                    // if (count >= 5) {
                    //     //console.log("In log:" + logObject.length)
                    //     //sendResponse()
                    //     return;
                    // }

                    // Show the commit sha.
                    //console.log("commit " + commit.sha());

                    // Store the author object.
                    var author = commit.author();

                    // Display author information.
                    //console.log("Author:\t" + author.name() + " <" + author.email() + ">");

                    // Show the commit date.
                    //console.log("Date:\t" + commit.date());

                    // Give some space and show the message.
                    //console.log("\n    " + commit.message());

                    logObject.push({ 'commitId': commit.sha(), 'authorName': author.name(), 'message': commit.message() })
                    commitsList.push(commit.sha())
                    count++;
                    // console.log(logObject.length)
                });

                // Start emitting events.
                history.start();
                // console.log(logObject.length)
                // return (logObject)
            }).catch(function (err) {
                console.log("Error in log:" + err)
            })
    })



}
