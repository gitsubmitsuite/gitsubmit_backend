var Git = require("../../../node_modules/nodegit");
var path = require('path')
module.exports = function (repo, fn) {
    Git.Repository.open(repo).then(function () {
        fn(true)
    }).catch(function (err) {
        fn(false)
    })
}