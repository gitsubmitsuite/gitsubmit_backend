
var Git = require("../../../node_modules/nodegit");
var path = require('path')


module.exports = function (repoUrl, folderDestination, fn) {
  var sshPublicKeyPath = require("path").join(__dirname, "../../../SSHKeys/id_rsa.pub");
  var sshPrivateKeyPath = require("path").join(__dirname, "../../../SSHKeys/id_rsa");
  var keyPhrase = "empty"
  var repository;
  // Open a repository that needs to be fetched and fast-forwarded
  Git.Repository.open(folderDestination)
    .then(function (repo) {
      repository = repo;
      // console.log("PULLING:" + repo.workdir())
      return repository.fetchAll({
        callbacks: {
          credentials: function (repoUrl, userName) {
            return Git.Cred.sshKeyNew(
              userName,
              sshPublicKeyPath,
              sshPrivateKeyPath,
              keyPhrase);
          },
          certificateCheck: function () {
            return 1;
          }
        }
      });
    })
    // Now that we're finished fetching, go ahead and merge our local branch
    // with the new one
    .then(function () {
      //console.log('MERGING with remote')
      return repository.mergeBranches("master", "origin/master");
    })
    .then(function (oid) {
      //console.log(oid)
      fn(true, { 'message': 'Successfully merged remote branch with local' })
    })
    .catch(function (err) {
      console.log("HAS CONFLICTS:" + err.hasConflicts() + " with error:" + err)
      var filesWithConflict = [];
      if (err.hasConflicts() == 1) {
        var arrayIndexEntries = err.entries();
        var files = ""
        for (var i = 0; i < arrayIndexEntries.length; i++) {
          //console.log(arrayIndexEntries[i].path)
          if (filesWithConflict.indexOf(arrayIndexEntries[i].path) < 0) {
            //File not in the array
            filesWithConflict.push(arrayIndexEntries[i].path)
          }

        }

        for (var i = 0; i < filesWithConflict.length; i++) {

          if (i == [filesWithConflict.length - 1]) {
            files += filesWithConflict[i]
          }
          else if (filesWithConflict.length == 1) {
            files = filesWithConflict[i]
          }
          else {
            files += filesWithConflict[i] + ","
          }

          console.log(files)
        }
        console.log("Sending the message")
        fn(false, { 'message': "Merge conflict in :" + files + ". Fix conflicts and then commit the result." })
      }
      else {
        fn(false, { 'message': "Error" })
      }

    })


}