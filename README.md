gitsubmit_backend


# Outline

 This project is a part of the Git Submit Suite, which helps users on the server side and to give assistance to the front-end developers.

# Description

Back-end code adds utility to everything the front-end designer creates.
Back-end is a combination of a database and a software written in a server-side language, which are run on the web servers, cloud-based servers or both.
The server-side application directly interacts with the database via an application programming interface (API) which pulls, saves or change data.
The data are returned and converted into front-end code a user interacts with.
In general, anything you see on an application is made possible by back-end code.
Back-end as a service is an exciting and extremely powerful approach for building client-server applications.



# Application Dependencies

This application uses Node.js.  Node.js is an open-source, cross-platform JavaScript runtime environment for developing a diverse variety of tools and applications. Although Node.js is not a JavaScript framework, many of its basic modules are written in JavaScript, and developers can write new modules in JavaScript. The runtime environment interprets JavaScript using Google's V8 JavaScript engine.

#  Datastores of a Git Repository
    This is an brief description about  [datastores of a git repository](https://bitbucket.org/gitsubmitsuite/gitsubmit_backend/wiki/Datastores%20of%20a%20Git%20Repository)