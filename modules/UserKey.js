

    UserKeys.prototype.addKey = function(userId, title, key, fn) {
      var params;
      if (fn == null) {
        fn = null;
      }
      params = {
        title: title,
        key: key
      };
      return this.post("users/" + userId + "/keys", params, function(data) {
        if (fn) {
          return fn(data);
        }
      });
    };

    return UserKeys;

  })(BaseModel);

  module.exports = function(client) {
    return new UserKeys(client);
  };

}).call(this);