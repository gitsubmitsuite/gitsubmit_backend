const Config = require('../../../node_modules/electron-config');
const config = new Config();
var Gitlab = require('../../../node_modules/gitlab');

module.exports = function (fn) {
    

    gitlab = new Gitlab({
        url: "http://csgrad06.nwmissouri.edu",
        token: config.get('private_token')
    });
   
    var returnedResults = [];
    gitlab.projects.all(function (projects) {
        var i, len, project, results;
        results = []
        for (i = 0, len = projects.length; i < len; i++) {
            project = projects[i];
            results.push("#" + project.id + ": " + project.name + ", path: " + project.path + ", default_branch: " + project.default_branch + ", private: " + project["private"] + ", owner: " + project.owner.name + " (" + project.owner.email + "), date: " + project.created_at);
           
    }
        
       
      returnedResults = results;
       callbackResults(returnedResults)
  
    })
    function callbackResults (returnedResults) {
        
         fn(returnedResults);
    }
   
    
   
}