(function() {
  var BaseModel, Users,
    bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  BaseModel = require('../BaseModel');

  Users = (function(superClass) {
    extend(Users, superClass);

    function Users() {
      this.search = bind(this.search, this);
      this.session = bind(this.session, this);
      this.create = bind(this.create, this);
      this.show = bind(this.show, this);
      this.current = bind(this.current, this);
      this.all = bind(this.all, this);
      this.init = bind(this.init, this);
      return Users.__super__.constructor.apply(this, arguments);
    }

    Users.prototype.init = function() {
      return this.keys = this.load('UserKeys');
    };

    
    return Users;

  })(BaseModel);

  module.exports = function(client) {
    return new Users(client);
  };

})