
var Git = require("../../../node_modules/nodegit");
var path = require('path')
var local = path.join.bind(path, __dirname);

module.exports = function (repoUrl, folderDestination, project_name, fn) {

  // Remote URL from GitLab server
  //var pullURL = "git@csgrad06.nwmissouri.edu:F2017-4450.ADB/msardar_gitlabClone.git"; 
  var pullURL = git@csgrad06.nwmissouri.edu:F2017-4450.ADB/msardar_gitlabClone.git ;
  
  // Pull a given repository into the desired folder.
 // var localPath = require("path").join(__dirname, "../../../CLONETEST");
  var localPath = path.join(folderDestination + "//" + project_name)
  var sshPublicKeyPath = require("path").join(__dirname, "../../../../.ssh/id_rsa.pub");
  var sshPrivateKeyPath = require("path").join(__dirname, "../../../../.ssh/id_rsa");
  var keyPhrase = "empty"

  
  var opts = {
    fetchOpts: {
      callbacks: {
        certificateCheck: function () {
          return 1;
        },
        credentials: function (pullURL, userName) {
          return Git.Cred.sshKeyNew(
            userName,
            sshPublicKeyPath,
            sshPrivateKeyPath,
            keyPhrase);
        }
      }
    }
  };

  //Calling pull function
 var cloneRepository = Git.Clone(cloneURL, localPath, opts)
    .then(function () {
      console.log("*********Pull success*********")
      fn(true, { message: "Successfully pulled the repo" })
    }).catch(function (err) {
      console.log(err);
      if (err.toString().indexOf('exists') != -1) {

        fn(false, { message: "Folder exists and is not an empty directory" })
      }
      else {
        fn(false, { message: "Error: Cannot pulled the repository due to:" + err })
      }

    });
	
	
	
 	// Open a repository that needs to be fetched and fast-forwarded
nodegit.Repository.open(path.resolve(__dirname, repoDir))
  .then(function(repo) {
    repository = repo;

    return repository.fetchAll({
      callbacks: {
        credentials: function(url, userName) {
          return nodegit.Cred.sshKeyFromAgent(userName);
        },
        certificateCheck: function() {
          return 1;
        }
      }
    });
  })
  // Now that we're finished fetching, go ahead and merge our local branch
  // with the new one
  .then(function() {
    return repository.mergeBranches("master", "origin/master");
  })
  .done(function() {
    console.log("Done!");
  });




}