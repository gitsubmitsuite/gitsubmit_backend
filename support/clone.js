var Git = require("../../../node_modules/nodegit");
var path = require('path')
var local = path.join.bind(path, __dirname);

module.exports = function () {

  // Remote URL from GitLab server
  var cloneURL = "git@csgrad06.nwmissouri.edu:F2017-4450.ADB/msardar_gitlabClone.git";
  // Clone a given repository into the desired folder.
  var localPath = require("path").join(__dirname, "../../../CLONETEST");
  var cloneOptions = {};
  //Set this path to the desired location and make sure both private and public keys exist in the path
  var sshPublicKeyPath = require("path").join(__dirname, "../../../../../../../../../../.ssh/id_rsa.pub");
  var sshPrivateKeyPath = require("path").join(__dirname, "../../../../../../../../../../.ssh/id_rsa");
  var keyPhrase = "empty"
  //Setting the options for clone
  var opts = {
    fetchOpts: {
      callbacks: {
        certificateCheck: function () {
          return 1;
        },
        credentials: function (cloneURL, userName) {
          return Git.Cred.sshKeyNew(
            userName,
            sshPublicKeyPath,
            sshPrivateKeyPath,
            keyPhrase);
        }
      }
    }
	  

    gitlab = new Gitlab({
        url: "http://csgrad06.nwmissouri.edu",
        token: config.get('private_token')
    });
   
    var returnedResults = [];
    gitlab.projects.all(function (projects) {
        var i, len, project, results;
        results = []
        for (i = 0, len = projects.length; i < len; i++) {
            project = projects[i];
            results.push("#" + project.id + ": " + project.name + ", path: " + project.path + ", default_branch: " + project.default_branch + ", private: " + project["private"] + ", owner: " + project.owner.name + " (" + project.owner.email + "), date: " + project.created_at);
           
    }
        
       
      returnedResults = results;
       callbackResults(returnedResults)
  
    })
    function callbackResults (returnedResults) {
        
         fn(returnedResults);
    }
  };
