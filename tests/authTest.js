var chai = require('chai');
var chaiHttp = require('chai-http');
// Interesting part
var app = require('../server/server');
var loginUser = require('./login.js');
var auth = {token: ''};

chai.use(chaiHttp);
chai.should();

describe('/users', function() {

  beforeEach(function(done) {
    loginUser(auth, done);
  });

  it('returns users as JSON', function(done) {
    // This is what launch the server
    chai.request(app)
    .get('/api/users')
    .set('Authorization', auth.token)
    .then(function (res) {
      res.should.have.status(200);
      res.should.be.json;
      res.body.should.be.instanceof(Array).and.have.length(1);
      res.body[0].should.have.property('username').equal('admin');
      done();
    })
    .catch(function (err) {
      return done(err);
    });
  });
});