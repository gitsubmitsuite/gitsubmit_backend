const Config = require('../../../node_modules/electron-config');
const config = new Config();
var Gitlab = require('../../../node_modules/gitlab');
module.exports = function (fn) {
    //console.log("I am in list-projects")
    var gitlab = new Gitlab({
        url: "http://csgrad06.nwmissouri.edu",
        token: config.get('private_token')
    });
    //console.log("Before all() call")
    var returnedResults = [];
    gitlab.projects.all(function (projects) {
        var i, len, project, results;
        results = []
        for (i = 0, len = projects.length; i < len; i++) {
            project = projects[i];
            var split_result = split_namespace_result(project.path_with_namespace)
            results.push({ 'id': project.id, 'project_name': project.name, 'path_with_namespace': project.path_with_namespace, 'ssh_url': project.ssh_url_to_repo, 'split_namespace_result': split_result, 'created_at': project.created_at });
            console.log(results[i])
        }

        // console.log("In for"+returnedResults.length)
        returnedResults = results;
        callbackResults(returnedResults)
        // return results;
    })
    function callbackResults(returnedResults) {
        // console.log("After all() call")
        //console.log(returnedResults.length)
        fn(returnedResults);
    }
    function split_namespace_result(namespace) {

        var semester = "";
        var project = "";
        var course = "";
        var section = "";
        var courseName = "";

        if (namespace.split('-').length <= 1) {
            console.log("Inapproppriate namespace URL. Sending emtpy strings for semester,project,course.")
        }
        else {
            // semester = namespace.split('-')[0]
            // course = namespace.split('-')[1].split('/')[0]
            // project = namespace.split('-')[1].split('/')[1]
            project = namespace.split('/')[1]
            semester = namespace.split('-')[0]
            course = namespace.split('-')[1]
            section = namespace.split('.')[0].split('-')[2]
            courseName = namespace.split('.')[1].split('/')[0]
        }
        console.log(semester, course, section, courseName, project)
        return { 'semester': semester, 'course': course, 'section': section, 'courseName': courseName, 'project': project }

    }
    // console.log("I am out")
    //EXCEUTES FIRST

    // returnedResults.push("Hello");


}
