var Git = require("../../../node_modules/nodegit");
var path = require('path')

module.exports = function (fn) {
    var logObject = [];
    // Open the repository directory.
    // Create a counter to only show up to 9 entries.
    var count = 0;
    Git.Repository.open(path.resolve(__dirname, "../../../../../CLONETEST/msardar_gitlabClone/.git"))
        // Open the master branch.

        .then(function (repo) {
            return repo.getMasterCommit();
        })
        // Display information about commits on master.
        .then(function (firstCommitOnMaster) {
            // Create a new history event emitter.
            var history = firstCommitOnMaster.history();


            history.on('end', function (commits) {
                console.log("In log:" + logObject.length)
                fn(true, logObject)
                // Use commits
            });
            // Listen for commit events from the history.
            history.on("commit", function (commit) {
                // Disregard commits past 5.
                if (count >= 5) {
                    //console.log("In log:" + logObject.length)
                    //sendResponse()
                    return;
                }

                // Show the commit sha.
                // console.log("commit " + commit.sha());

                // Store the author object.
                // var author = commit.author();

                // Display author information.
                //  console.log("Author:\t" + author.name() + " <" + author.email() + ">");

                // Show the commit date.
                //console.log("Date:\t" + commit.date());

                // Give some space and show the message.
                //console.log("\n    " + commit.message());

                logObject.push({ 'commitId': commit.sha() })
                count++;
                // console.log(logObject.length)
            });

            // Start emitting events.
            history.start();
            // console.log(logObject.length)
            // return (logObject)
        }).catch(function (err) {
            console.log("Error in log:" + err)
        })


}
