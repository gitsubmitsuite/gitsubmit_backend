var Git = require("../../../node_modules/nodegit");
var path = require('path')

module.exports = function (folder, fn) {
	var response = []
	// This code shows working directory changes similar to git status

	//Git.Repository.open(path.resolve(__dirname, "../../../../../CLONETEST/msardar_gitlabClone/.git"))
	Git.Repository.open(path.resolve(folder))
		.then(function (repo) {
			console.log("REPO OPENED")
			repo.getStatus().then(function (statuses) {
				console.log("Got status")
				function statusToText(status) {
					var words = [];
					if (status.isNew()) { words.push("NEW"); }
					if (status.isModified()) { words.push("MODIFIED"); }
					if (status.isTypechange()) { words.push("TYPECHANGE"); }
					if (status.isDeleted()) { words.push("DELETED"); }
					if (status.isRenamed()) { words.push("RENAMED"); }
					if (status.isIgnored()) { words.push("IGNORED"); }

					return words.join(" ");
				}

				statuses.forEach(function (file) {
					//	console.log(file.path() + " " + statusToText(file));
					response.push({ 'fileName': file.path(), 'status': statusToText(file) })
				});
				//console.log(response.length)

				fn(response)
			}).catch(function (err) {
				console.log("Status error: " + err);
			})


		}).catch(function (err) {
			console.log("Status error: " + err);


		});
}	